Java Multi Module Skeleton
=====================
This is a lean skeleton of a multi module java gradle project that contains Api, Common, Data, and Web modules. Project built with Java 8, MySQL 5, Gradle 4, Spring Boot 1.5.4, JPA, Log4j, and JUnit.

## Requirements
- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [MySQL 5](https://www.mysql.com/downloads/)
- [Gradle 4](https://gradle.org/install/) (optional, gradle wrapper included in project)

## Configuration
MySQL username and password stored in application.properties files. You can change them by your MySQL credentials.


## Development Environment
You can pass **-Dspring.profiles.active=dev** VM Option to run the project in development stage. If you do this *application-dev.properties*, and *log4j-dev.properties* files will be enabled.

Additionally you can run project with gradle as below;
```sh
$ gradle :api:bootRun -Dspring.profiles.active=dev -Dlog4j.configuration=log4j-dev.properties
$ gradle :web:bootRun -Dspring.profiles.active=dev -Dlog4j.configuration=log4j-dev.properties
```

### Run MySQL as a Docker Container
You can run MySQL as a docker container in your development environment as below;
```sh
$ docker run --name mysql --net=host -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 -d mysql:5.7.19
```

## Production Environment
You can leave VM Options sections empty to run the project in your production environment. Use any commands as below to run in your production environment with the settings specified in *application.properties*, and *log4j.properties* files.
```sh
$ gradle :api:bootRun
$ gradle :web:bootRun
```

### License

[Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0)


DISCLAIMER
----------
Please note: all tools/ scripts in this repo are released for use "AS IS" **without any warranties of any kind**,
including, but not limited to their installation, use, or performance.  We disclaim any and all warranties, either 
express or implied, including but not limited to any warranty of noninfringement, merchantability, and/ or fitness 
for a particular purpose.  We do not warrant that the technology will meet your requirements, that the operation 
thereof will be uninterrupted or error-free, or that any errors will be corrected.

Any use of these scripts and tools is **at your own risk**.  There is no guarantee that they have been through 
thorough testing in a comparable environment and we are not responsible for any damage or data loss incurred with 
their use.

You are responsible for reviewing and testing any scripts you run *thoroughly* before use in any non-testing 
environment.

Thanks,   
[Ergin BULUT](https://www.erginbulut.com)