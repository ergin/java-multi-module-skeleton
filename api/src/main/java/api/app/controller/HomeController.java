package api.app.controller;

import api.app.model.SampleModel;
import data.sql.entities.Member;
import data.sql.repositories.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @Autowired
    private MemberRepository memberRepository;

    private static final String email = "ergin@example.com";

    @RequestMapping("/")
    public SampleModel index() {
        Member member = memberRepository.findByEmail(email);
        if(member == null) {
            member = new Member();
            member.setAuthToken("TOKEN");
            member.setEmail(email);
            member.setFirstName("Ergin");
            member.setLastName("BULUT");
            member.setGender("M");
            member.setPassword("PASSWD");
            member.setRole("ADMINISTRATOR");

            memberRepository.save(member);
        }

        return new SampleModel(member.toString(), member.getMemberId());
    }
}