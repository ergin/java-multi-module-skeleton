package data.sql.entities;

import data.sql.entities.base.BaseEntity;
import data.sql.enums.MemberStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Entity
@Table(
        name = "Members",
        indexes = {
                @Index(name = "UK_Members_Email", columnList = "Email", unique = true),
                @Index(name = "UK_Members_MemberGuid", columnList = "MemberGuid", unique = true)
        }
)
public class Member extends BaseEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MemberId")
    public int memberId;
    @Column(name = "MemberGuid", nullable = false, updatable = false)
    private UUID memberGuid;
    @Column(name = "FirstName", length = 64)
    private String firstName;
    @Column(name = "LastName", length = 64)
    private String lastName;
    @Column(name = "Email", nullable = false, length = 128)
    private String email;
    @Column(name = "Password", nullable = false, length = 64)
    private String password;
    @Column(name = "AuthToken", nullable = false, length = 64)
    private String authToken;
    @Column(name = "Gender", nullable = false)
    private String gender;
    @Column(name = "Role", nullable = false, length = 16)
    private String role;
    @Enumerated(EnumType.STRING)
    @Column(name = "Status", nullable = false, length = 16)
    private MemberStatus status;

    @OneToMany(mappedBy = "member", fetch = FetchType.LAZY)
    private List<MemberAttribute> attributes;

    public Member() {
    }

    public Member(Integer memberId) {
        this.memberId = memberId;
    }

    public Member(UUID memberGuid) {
        this.memberGuid = memberGuid;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public UUID getMemberGuid() {
        return memberGuid;
    }

    public void setMemberGuid(UUID memberGuid) {
        this.memberGuid = memberGuid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public MemberStatus getStatus() {
        return status;
    }

    public void setStatus(MemberStatus status) {
        this.status = status;
    }

    public List<MemberAttribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<MemberAttribute> attributes) {
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        return getFirstName() + " " + getLastName();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Member)) {
            return false;
        }
        Member other = (Member) obj;
        return getMemberId() == other.getMemberId();
    }
}
