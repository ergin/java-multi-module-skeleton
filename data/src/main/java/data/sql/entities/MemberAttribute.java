package data.sql.entities;

import data.sql.entities.base.BaseAttribute;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "MemberAttributes")
public class MemberAttribute extends BaseAttribute implements Serializable {
    private static final long serialVersionUID = 1L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MemberId", foreignKey = @ForeignKey(name = "FK_Members_MemberId"))
    private Member member;

    public MemberAttribute() {
    }

    public MemberAttribute(Member member, String attribute, String content) {
        this.member = member;
        this.setAttribute(attribute);
        this.setContent(content);
    }

    @Override
    public String toString() {
        return this.getContent();
    }
}
