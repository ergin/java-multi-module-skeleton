package data.sql.enums;

public enum MemberStatus {
    ACTIVE,
    PASSIVE,
    NOTCONFIRMED,
    BLOCKED,
    DELETED
}
