package data.sql.repositories;

import data.sql.entities.Member;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface MemberRepository extends PagingAndSortingRepository<Member, Integer> {
    Member findByEmail(String email);
    List<Member> findByRole(String role);
}