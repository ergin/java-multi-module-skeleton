package web.app.controller;

import data.sql.entities.Member;
import data.sql.repositories.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.ui.Model;
import web.app.model.SampleModel;

@Controller
public class HomeController {
	// inject via application-dev.properties or application.properties depends on your VM Options.
	@Value("${welcome.message}")
	private String message = "";

	@Autowired
	private MemberRepository memberRepository;

	@RequestMapping("/")
    public String index(Model model) {
        SampleModel sampleModel = new SampleModel(message);

        Member member = memberRepository.findByEmail("erginbulut@gmail.com");
        if (member != null) sampleModel.setId(member.getMemberId());

		model.addAttribute("message", sampleModel.toString());
		return "index";
	}
}