package web.app.model;
 
public class SampleModel {
    private String message;
    private Integer id;
 
    public SampleModel() {
    }
 
    public SampleModel(String message) {
        this.message = message;
    }

    public SampleModel(String message, Integer id) {
        this.message = message;
        this.id = id;
    }
 
    public String getMessage() {
        return message;
    }
 
    public void setMessage(String message) {
        this.message = message;
    }
 
    public Integer getId() {
        return id;
    }
 
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return String.format(message, id);
    }
}