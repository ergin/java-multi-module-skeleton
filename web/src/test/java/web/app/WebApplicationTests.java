package web.app;

import common.libs.utils.Randomizer;
import data.sql.entities.Member;
import data.sql.enums.MemberStatus;
import data.sql.enums.RowStatus;
import data.sql.repositories.MemberRepository;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WebApplicationTests {
    private final static Logger log = Logger.getLogger(WebApplicationTests.class);
    private final static String email = "ergin@example.com";

    @Autowired
    private MemberRepository memberRepository;

    @Before
    public void setUp() {
        Member member = memberRepository.findByEmail(email);
        if (member == null) {
            member = new Member();
            member.setAuthToken(Randomizer.randomString(16));
            member.setCreatedBy(1);
            member.setEmail(email);
            member.setFirstName("Ergin");
            member.setGender("M");
            member.setLastName("BULUT");
            member.setMemberGuid(UUID.randomUUID());
            member.setPassword("PASSWD");
            member.setRole("ADMINISTRATOR");
            member.setRowStatus(RowStatus.ACTIVE);
            member.setStatus(MemberStatus.ACTIVE);

            memberRepository.save(member);
        }
    }

    @Test
    public void memberShouldBeExist() {
        Member member = memberRepository.findByEmail(email);

        assertThat(member).isNotNull();
        assertThat(member.getEmail()).isEqualTo(email);
        assertThat(member.getMemberId()).isGreaterThan(0);
    }
}
